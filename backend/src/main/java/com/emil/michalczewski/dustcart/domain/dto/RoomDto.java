package com.emil.michalczewski.dustcart.domain.dto;

import lombok.Data;
import java.util.List;

@Data
public class RoomDto {
    private String id;
    private String number;
    private List<String> usersUuids;
}
