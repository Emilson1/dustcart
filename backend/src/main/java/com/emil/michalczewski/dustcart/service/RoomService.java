package com.emil.michalczewski.dustcart.service;

import com.emil.michalczewski.dustcart.domain.model.Room;
import com.emil.michalczewski.dustcart.domain.model.Task;
import com.emil.michalczewski.dustcart.domain.model.User;
import com.emil.michalczewski.dustcart.domain.repository.RoomRepository;
import com.emil.michalczewski.dustcart.domain.repository.TaskRepository;
import com.emil.michalczewski.dustcart.domain.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class RoomService {

    private RoomRepository roomRepository;
    private TaskRepository taskRepository;
    private UserRepository userRepository;


    public RoomService(RoomRepository roomRepository, TaskRepository taskRepository, UserRepository userRepository) {
        this.roomRepository = roomRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    //TODO Retry on OptimisticLockingException
    public void assignUserToRoom(String roomNumber, String username){
        Room room = roomRepository.findByNumber(roomNumber);
        User user = userRepository.findByUsername(username);
        if(room != null) {
            room.getUsersUuids().add(user.getUuid().toString());
            roomRepository.save(room);
        }
    }

    public void addTaskToRoom( String roomNumber, Task task){
        Room room = roomRepository.findByNumber(roomNumber);
        if(room!=null){
            Task addedTask = taskRepository.insert(task);
            room.getTasksIds().add(addedTask.getId());
            roomRepository.save(room);
        }
    }




}
