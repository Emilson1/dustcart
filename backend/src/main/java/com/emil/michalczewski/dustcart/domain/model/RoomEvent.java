package com.emil.michalczewski.dustcart.domain.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Emil Michalczewski on 23.02.17.
 */
@Document(collection = "events.RoomEvents")
@Data
@Builder
public class RoomEvent {
    @Id
    private String id;
    private Date eventDate;
    private UUID userUuid;
    private String roomId;
}
