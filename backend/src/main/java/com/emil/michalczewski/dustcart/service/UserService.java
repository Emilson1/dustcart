package com.emil.michalczewski.dustcart.service;

import com.emil.michalczewski.dustcart.domain.dto.UserDto;
import com.emil.michalczewski.dustcart.domain.model.Room;
import com.emil.michalczewski.dustcart.domain.model.User;
import com.emil.michalczewski.dustcart.domain.repository.RoomRepository;
import com.emil.michalczewski.dustcart.domain.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private RoomRepository roomRepository;
    private UserRepository userRepository;
    private ModelMapper mapper;

    @Autowired
    public UserService(RoomRepository roomRepository, UserRepository userRepository, ModelMapper mapper) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    public UserDto getUser(String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            Room userRoom = roomRepository.findByUsersUuidsContains(user.getUuid().toString());
            UserDto userDto = mapper.map(user, UserDto.class);
            if (userRoom != null) {
                userDto.setRoomNumber(userRoom.getNumber());
            }
            return userDto;
        }
        return null;
    }
}
