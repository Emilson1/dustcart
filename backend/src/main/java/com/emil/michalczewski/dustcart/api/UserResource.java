package com.emil.michalczewski.dustcart.api;

import com.emil.michalczewski.dustcart.domain.dto.NewUserDto;
import com.emil.michalczewski.dustcart.domain.dto.UserDto;
import com.emil.michalczewski.dustcart.domain.model.User;
import com.emil.michalczewski.dustcart.domain.repository.UserRepository;
import com.emil.michalczewski.dustcart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.UUID;


/**
 * Created by Emil Michalczewski on 2017-02-22.
 */
@RestController
public class UserResource {
    private UserRepository userRepository;
    private PasswordEncoder encoder;
    private UserService userService;

    @Autowired
    public UserResource(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody NewUserDto newUserDto) {
        encoder = new BCryptPasswordEncoder();
        User newUser = User.builder().email(newUserDto.getEmail())
                .password(encoder.encode(newUserDto.getPassword()))
                .username(newUserDto.getUsername())
                .uuid(UUID.randomUUID())
                .build();
        userRepository.insert(newUser);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/user")
    public ResponseEntity<?> getLoggedUser(Principal principal){
        UserDto user = userService.getUser(principal.getName());
        if(user == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(user);
    }
}
