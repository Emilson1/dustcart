package com.emil.michalczewski.dustcart.domain.repository;

import com.emil.michalczewski.dustcart.domain.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Emil Michalczewski on 23.02.17.
 */
public interface UserRepository extends MongoRepository<User,String> {
    User findByUsername(String username);
}
