package com.emil.michalczewski.dustcart.domain.repository;

import com.emil.michalczewski.dustcart.domain.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskRepository extends MongoRepository<Task,String> {
}
