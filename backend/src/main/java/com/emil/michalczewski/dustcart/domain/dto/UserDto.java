package com.emil.michalczewski.dustcart.domain.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserDto {
    private UUID uuid;
    private String username;
    private String email;
    private String roomNumber;
}
