package com.emil.michalczewski.dustcart.domain.dto;

import lombok.Data;

@Data
public class NewRoomDto {
    private String number;
}
