package com.emil.michalczewski.dustcart.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Emil Michalczewski on 23.02.17.
 */
@Configuration
public class ModelMapperConfiguartion {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
