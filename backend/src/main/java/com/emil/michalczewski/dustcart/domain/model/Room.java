package com.emil.michalczewski.dustcart.domain.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by Emil Michalczewski on 23.02.17.
 */
@Document(collection = "room.Rooms")
@Data
@Builder
public class Room {
    @Id
    private String id;
    @Version
    private Long version;
    @Indexed(unique = true)
    private String number;
    private List <String> usersUuids;
    private List <String> tasksIds;
}
