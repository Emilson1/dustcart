package com.emil.michalczewski.dustcart.api;

import com.emil.michalczewski.dustcart.domain.dto.NewRoomDto;
import com.emil.michalczewski.dustcart.domain.dto.RoomDto;
import com.emil.michalczewski.dustcart.domain.dto.TaskDto;
import com.emil.michalczewski.dustcart.domain.model.Room;
import com.emil.michalczewski.dustcart.domain.model.Task;
import com.emil.michalczewski.dustcart.domain.repository.RoomRepository;
import com.emil.michalczewski.dustcart.domain.repository.TaskRepository;
import com.emil.michalczewski.dustcart.service.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.stream.Collectors;

@RestController("api/room")
public class RoomController {

    private RoomRepository roomRepository;
    private ModelMapper mapper;
    private TaskRepository taskRepository;
    private RoomService roomService;

    @Autowired
    public RoomController(RoomRepository roomRepository, ModelMapper mapper, TaskRepository taskRepository, RoomService roomService) {
        this.roomRepository = roomRepository;
        this.mapper = mapper;
        this.taskRepository = taskRepository;
        this.roomService = roomService;
    }

    @PostMapping
    public void createRoom(NewRoomDto newRoomDto) {
        roomRepository.insert(Room.builder().number(newRoomDto.getNumber()).build());
    }

    @GetMapping("/{number}")
    public ResponseEntity<?> getRoom(@PathVariable("number") String roomNumber) {
        Room room = roomRepository.findByNumber(roomNumber);
        if (room != null) {
            return ResponseEntity.ok(mapper.map(room, RoomDto.class));
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{number}/tasks")
    public ResponseEntity<?> getRoomTasks(@PathVariable("number") String roomNumber) {
        Room room = roomRepository.findByNumber(roomNumber);
        if (room == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(room.getTasksIds().stream().
                map(id -> mapper.map(taskRepository.findOne(id), TaskDto.class))
                .collect(Collectors.toList()));

    }

    @PostMapping("/{number}/users")
    public ResponseEntity<?> assignUserToRoom(Principal principal, @PathVariable("number") String roomNumber) {
        roomService.assignUserToRoom(roomNumber, principal.getName());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{number}/tasks")
    public ResponseEntity<?> assignTaskToRoom(@PathVariable("number") String roomNumber, @RequestBody TaskDto taskDto) {
        roomService.addTaskToRoom(roomNumber, mapper.map(taskDto, Task.class));
        return ResponseEntity.ok().build();
    }
}
