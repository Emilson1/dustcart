package com.emil.michalczewski.dustcart.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TaskDto {
    private String name;
    private String description;
}
