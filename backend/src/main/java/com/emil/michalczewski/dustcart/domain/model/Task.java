package com.emil.michalczewski.dustcart.domain.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
public class Task {
    private String id;
    private String name;
    private String description;
}
