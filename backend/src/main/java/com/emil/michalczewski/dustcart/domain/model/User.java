package com.emil.michalczewski.dustcart.domain.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

/**
 * Created by Emil Michalczewski on 2017-02-22.
 */

@Document(collection = "user.Users")
@Data
@Builder
public class User {
    @Id
    private String id;
    private UUID uuid;
    private String username;
    private String email;
    private String password;
}
