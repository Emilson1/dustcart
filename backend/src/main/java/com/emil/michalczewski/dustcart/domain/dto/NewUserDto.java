package com.emil.michalczewski.dustcart.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Emil Michalczewski on 2017-02-22.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewUserDto {
    private String username;
    private String password;
    private String email;
}
