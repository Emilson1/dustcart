package com.emil.michalczewski.dustcart.domain.repository;

import com.emil.michalczewski.dustcart.domain.model.Room;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Emil Michalczewski on 23.02.17.
 */
public interface RoomRepository extends MongoRepository<Room, String> {
    Room findByNumber(String number);
    Room findByUsersUuidsContains(String userUuid);
}
