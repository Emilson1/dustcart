import {Component, OnInit} from '@angular/core';
import {User} from "../../model/User";
import {UserService} from "../../service/user.service";


@Component({
    selector: 'info',
    templateUrl: 'info.component.html',
})
export class InfoComponent implements OnInit{
    user: User = <User>{};

    ngOnInit(): void {
        this.userService.getUser().then(
            user => this.user = user
        )
    }

    constructor(private userService: UserService) {}
}