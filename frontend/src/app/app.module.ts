import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {LocationStrategy, HashLocationStrategy, APP_BASE_HREF} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AlertModule, DatepickerModule} from 'ng2-bootstrap';
import {routing, appRouterProviders} from './app.routing';
import {AppComponent} from './app.component';
import {AuthenticationService} from "./service/authentication.service";
import {LoginComponent} from "./login/login.component";
import {CanActivateAuthGuard} from "./can-activate.authguard";
import {HeaderComponent} from "./layout/header/header.component";
import {UserService} from "./service/user.service";
import {InfoComponent} from "./dashboard/info/info.component";

@NgModule({
    declarations: [AppComponent,
        LoginComponent,
        InfoComponent,
        HeaderComponent],
    imports: [BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AlertModule.forRoot(),
        DatepickerModule.forRoot(),
        routing],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        AuthenticationService,
        CanActivateAuthGuard,
        UserService,
        appRouterProviders,
        [{provide: APP_BASE_HREF, useValue: '/'}],
        [{provide: LocationStrategy, useClass: HashLocationStrategy}]
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
