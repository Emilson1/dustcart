import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-header',
    template: `           <nav>
        <a routerLink="/info" routerLinkActive="active">Info</a>
    </nav> `,
    styleUrls: []
})
export class HeaderComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}