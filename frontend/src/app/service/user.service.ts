import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {User} from "../model/User";
import {Headers} from '@angular/http';
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class UserService {
    private apiUrl = 'http://localhost:8080/user';

    private headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.authenticationService.getToken()
    });

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getUser(): Promise<User> {
        console.log(this.authenticationService.getToken());
        return this.http
            .get(this.apiUrl, {headers: this.headers})
            .toPromise()
            .then(response => response.json() as User)
            .catch(this.handleError);

    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred: ', error); // for demo only
        return Promise.reject(error.message || error);
    }
}