import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {CanActivateAuthGuard} from "./can-activate.authguard";
import {InfoComponent} from "./dashboard/info/info.component";

const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: '', redirectTo: '/info', pathMatch: 'full'},
    {path: 'info', component: InfoComponent, canActivate: [CanActivateAuthGuard]}
];

export const appRouterProviders: any[] = [];

export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);
